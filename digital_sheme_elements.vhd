process(clock)                               -- comparator with clock
begin
   if (clock'event and clock ='1') then
      if (input1 = input2) then
         output <= '1';
      else
         output <= '0';
      end if;
   end if;
end process;


process(clock)                               -- counter 
begin
   if <clock>='1' and <clock>'event then
      <count> <= <count> + 1;
   end if;
end process;

                                             -- multiplexer 4 to 1
process (<selector>,<input1>,<input2>,<input3>,<input4>)
begin
   case <selector> is
      when "00" => <output> <= <input1>;
      when "01" => <output> <= <input2>;
      when "10" => <output> <= <input3>;
      when "11" => <output> <= <input4>;
      when others => <output> <= <input1>;
   end case;
end process;

                                             -- decoder or encoder with clock
process(<clock>)
begin
   if ( <clock>'event and <clock> ='1') then
      if ( <reset> = '1') then
         <output> <= "0000";
      else
         case <input> is
            when "00" => <output> <= "0001";
            when "01" => <output> <= "0010";
            when "10" => <output> <= "0100";
            when "11" => <output> <= "1000";
            when others => <output> <= "0000";
         end case;
      end if;
   end if;
end process;

------------------------------------------------------------------------
-- simple ram

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rams_04 is
    port (CLK : in std_logic;
          WE  : in std_logic;
          A   : in std_logic_vector(5 downto 0);
          DI  : in std_logic_vector(15 downto 0);
          DO  : out std_logic_vector(15 downto 0));
end rams_04;

architecture syn of rams_04 is
    type ram_type is array (63 downto 0) of std_logic_vector (15 downto 0);
    signal RAM : ram_type;
begin

    process (CLK)
    begin
        if (CLK'event and CLK = '1') then
            if (WE = '1') then
                RAM(to_integer(unsigned(A))) <= DI;
            end if;
        end if;
    end process;

    DO <= RAM(to_integer(unsigned(A)));

end syn;

					
-- shift register
			
library ieee;
use ieee.std_logic_1164.all;

entity shift_registers_1 is
    port(CLK, SI : in std_logic;
         SO : out std_logic);
end shift_registers_1;

architecture archi of shift_registers_1 is
    signal tmp: std_logic_vector(7 downto 0);
begin

    process (CLK)
    begin
        if (CLK'event and CLK='1') then
            for i in 0 to 6 loop
                tmp(i+1) <= tmp(i);
            end loop;
            tmp(0) <= SI;
        end if;
    end process;

    SO <= tmp(7);

end archi;

				
				
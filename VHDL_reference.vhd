----------------------------------------------
-- Predefined Types of constants or signals --
----------------------------------------------

-- must to use:
STD_LOGIC              --'U','X','0','1','Z','W','L','H','-'
STD_LOGIC_VECTOR       --Natural Range of STD_LOGIC
INTEGER                --32 or 64 bits
BOOLEAN                --True or False

-- use mostly for verification:
NATURAL                --Integers >= 0
POSITIVE               --Integers > 0
REAL                   --Floating-point
BIT                    --'0','1'
BIT_VECTOR(Natural)    --Array of bits
CHARACTER              --7-bit ASCII
STRING(POSITIVE)       --Array of charachters
TIME                   --hr, min, sec, ms, us, ns, ps, fs
DELAY_LENGTH           --Time >= 0

--------------------------------------------
------------- library install part ---------
--------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;


--------------------------------------------
-- element interface ('black box') part ----
--------------------------------------------
entity entity_name is 
generic ( 								-- global constants or parameters
   generic_name1 : type := value1;
   generic_name2 : type := value2
);
port (
   port_name1 : in    std_logic;
   port_name2 : out   std_logic_vector(63 downto 0);
   port_name3 : inout std_logic_vector(31 downto 0)
);
end entity_name;


architecture arch_name of entity_name is 
--------------------------------------------
-- signal and component declarations part --
--------------------------------------------

	constant name1: integer := 56;

	signal name2: STD_LOGIC := '1';
	signal name3: std_logic_vector(31 downto 0):= x"A10300FF";
	signal name4: std_logic_vector(31 downto 0):= (others=>'0');

	type vector8 is array (0 to 3) of std_logic_vector (7 downto 0);
	signal ron1_bus : vector8:=(x"A1", x"A2", x"A3", x"A4");	
	signal ron2_bus : vector8:=(others=>(others=>'0'));

	--------------------------
	-- Don't do this! 
	type vector1 is array (0 to 31) of std_logic;
	signal some_bus : vector1;
	-- Do this!
	signal some_bus : std_logic_vector(31 downto 0);
	--------------------------

	component component_name
	generic (
	   generic_name : integer := 22
	);
	port (
		port_name1 : in    std_logic;
		port_name2 : out   std_logic_vector(63 downto 0);
		port_name3 : inout std_logic_vector(31 downto 0)
	 );
	end component;
			
begin
--------------------------------------------
------------ logic install part ------------
--------------------------------------------

-- Some VHDL operators 

+   ---- Addition
-   ---- Subtraction
*   ---- Multiplication
/   ---- Divide
mod ---- Modulus
**  ---- Power Operator (i.e. 2**8 returns 256)

NOT  ---- Invert a single-bit signal or each bit in a bus		---- Not True
AND  ---- AND two single bits or each bit between two buses		---- Both Inputs True
OR   ---- OR two single bits or each bit between two buses		---- Either Input True
XOR  ---- XOR two single bits or each bit between two buses
XNOR ---- XNOR two single bits or each bit between two buses
 
=   ---- Inputs Equal
/=  ---- Inputs Not Equal
<   ---- Less-than
<=  ---- Less-than or Equal
>   ---- Greater-than
>=  ---- Greater-than or Equal

a & b & c 	---- Concatenate a, b and c into a bus

A'high 		---- value of the highest bit of bus
A'low  		---- value of the lowest bit of bus

A'left		---- value of the left bit of bus
A'right		---- value of the right bit of bus

A'length	---- length of bus^ 32
A'range		---- range of bus: (0 to 31) or (31 downto 0) 

---------------------------------------------

	instance_name1 : component_name
	generic map (generic_name => 11)
	port map (port_name1 => signal_name1; port_name2 => signal_name2; port_name3 => signal_name3);

	instance_name2 : component_name
	generic map (
		generic_name => 11
		)
	port map (
		port_name1 => signal_name1; 
		port_name2 => signal_name2; 
		port_name3 => signal_name3
		);

--------------------------------------------

output1 <= '1'    WHEN input1 = input2 ELSE '0';	-- comparator
output2 <= input1 WHEN selector ='1'   ELSE input2; -- multiplexor 2x1
	
end arch_name;		
			
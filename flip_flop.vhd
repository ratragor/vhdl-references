-- D flip-flop / register

----------- rising_edge -----------------
process (clock)
begin
	if (clock'event and clock = '1') then
		Q <= D;
	end if;
end process;

process (clock)
begin
	if rising_edge(clock) then
		Q <= D;
	end if;
end process;

Q <= D when rising_edge(clock);

----------- falling_edge -----------------

process (clock)
begin
	if (clock'event and clock = '0') then
		Q <= D;
	end if;
end process;

process (clock)
begin
	if falling_edge(clock) then
		Q <= D;
	end if;
end process;

Q <= D when falling_edge(clock);

----------------------------------------------------
-- D flip-flop / register with asincronius reset

process (clock)
begin
	if reset = '1' then
		Q <= (others=>'0');
	elsif (clock'event and clock = '1') then
		Q <= D;
	end if;
end process;

process (clock)
begin
	if reset = '1' then
		Q <= (others=>'0');
	elsif rising_edge(clock) then
		Q <= D;
	end if;
end process;

----------------------------------------------------
-- D flip-flop / register with asincronius set

process (clock)
begin
	if reset = '1' then
		Q <= (others=>'1');
	elsif (clock'event and clock = '1') then
		Q <= D;
	end if;
end process;

process (clock)
begin
	if reset = '1' then
		Q <= (others=>'1');
	elsif rising_edge(clock) then
		Q <= D;
	end if;
end process;

----------------------------------------------------
-- D flip-flop / register with sincronius set or reset

process (clock)
begin
	if (clock'event and clock = '1') then
		if reset = '1' then
			Q <= (others=>'0'); -- reset
		else
			Q <= D;
		end if;
	end if;
end process;

process (clock)
begin
	if rising_edge(clock) then
		if reset = '1' then
			Q <= (others=>'1'); -- set
		else
			Q <= D;
		end if;
	end if;
end process;

----------------------------------------------------
-- D flip-flop / register with sincronius set or reset and clock enable (CE)

process (clock)
begin
	if (clock'event and clock = '1') then
		if reset = '1' then
			Q <= (others=>'0'); -- reset
		elsif CE = '1' then
			Q <= D;
		end if;
	end if;
end process;

process (clock)
begin
	if rising_edge(clock) then
		if reset = '1' then
			Q <= (others=>'1'); -- set
		elsif CE = '1' then
			Q <= D;
		end if;
	end if;
end process;
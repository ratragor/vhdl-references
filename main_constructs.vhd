---------------------------------
-- стр.51 в методичке - сдвиги --
---------------------------------
QO(7 downto 0) <= DI(7)     & DI(7 downto 1); -- арифметический (A) 	
QO(7 downto 0) <= (N xor V) & DI(7 downto 1); -- модифицированный арифметический (Ma)
QO(7 downto 0) <= '0'       & DI(7 downto 1); -- логический с установкой 1 (L.1) или 0 (L.0)	
QO(7 downto 0) <= DI(0)     & DI(7 downto 1); -- циклический (С)					
QO(7 downto 0) <= DI(0)     & DI(7 downto 1); C <= DI(0); -- циклический через признак С (CC)		


---------------------------------
----- work out of process -------
---------------------------------

LABEL_1: if a = b generate begin
	d <= e;
end generate;	

LABEL_2: if a /= b generate begin
	d <= c;
end generate;	

--------------------------------

LABEL_3: for idx in 0 to 10 generate begin
	d(idx) <= e(idx);
end generate;   

---------------------------------
-------- work in process --------
---------------------------------

process (clock)
begin


   if <condition> then
      <statement>
   elsif <condition> then
      <statement>
   else
      <statement>
   end if;

				
   <name> <= <expression> when <condition> else
             <expression> when <condition> else
             <expression>;

			 
   case (<2-bit select>) is
      when "00" => <statement>;
      when "01" => <statement>;
      when "10" => <statement>;
      when "11" => <statement>;
      when others => <statement>;
   end case;


   with <choice_expression> select
      <name> <= <expression> when <choices>,
                <expression> when <choices>,
                <expression> when others;

------------------------------------------------

   for idx in 0 to 10 loop
      d(idx) <= e(idx);
   end loop;
   
------------------------------------------------

   while a /= b loop
      d(idx) <= e(idx);
   end loop;


end process;
			
	
				